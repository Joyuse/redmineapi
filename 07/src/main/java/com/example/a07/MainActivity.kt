package com.example.a07

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.agenda -> Log.w("Agenda", "Agenda Тык")
            R.id.item1 -> Log.w("Item 1", "Item 1 Тык")
            R.id.call -> Log.w("Call", "Call Тык")
            R.id.some -> Log.w("Some", "Some Тык")
        }
        return super.onOptionsItemSelected(item)
    }
}