package com.example.a08

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView



class MainActivity : AppCompatActivity(), OnClickListener {
    @SuppressLint("ResourceAsColor")

    lateinit var buttonPro : Button
    lateinit var buttonPro2 : Button
    lateinit var textViewPro: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val linearLayout = LinearLayout(this)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT)
        linearLayout.setBackgroundColor(Color.CYAN)
        linearLayout.addView(createTextView())
        linearLayout.addView(createButtonPro())
        linearLayout.addView(createButtonPro2())
        buttonPro = createButtonPro()
        buttonPro2 = createButtonPro2()
        //buttonPro2.setOnClickListener(this)
        setContentView(linearLayout)
    }

    fun createTextView(): TextView {
        val textViewPro = TextView(this)
        textViewPro.text = "TextViewPro"
        return textViewPro
    }

    @SuppressLint("ResourceType")
    fun createButtonPro() : Button {
        val buttonPro = Button(this)
        buttonPro.text = "Button Pro"
        buttonPro.tag = "SomeButton"
        buttonPro.id = 1
        buttonPro.setBackgroundColor(Color.DKGRAY)
        buttonPro.setOnClickListener(this)
        return  buttonPro
    }

    @SuppressLint("ResourceType")
    fun createButtonPro2() : Button {
        val buttonPro2 = Button(this)
        buttonPro2.text = "Button Pro2"
        buttonPro2.tag = "SomeButton2"
        buttonPro2.id = 2
        buttonPro2.setBackgroundColor(Color.BLUE)
        buttonPro2.setOnClickListener(this)
        return  buttonPro2
    }

    override fun onClick(v: View?) {
        Log.w("tah","work-1")
        when(v?.tag as String){
            buttonPro.tag -> {
                Log.w("tah","work-2")
                createTextView().text = "SomeText"
            }
            buttonPro2.tag ->{
                Log.w("tah","work-3")
                createTextView().text   = "Button2 Pro Clicked"
            }
        }
    }
}