package com.example.a10

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText



class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var button: Button
    lateinit var button_open_Browser: Button
    lateinit var textField_name: EditText
    lateinit var button_map: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textField_name = findViewById(R.id.editTextTextPersonName)
        button_open_Browser = findViewById(R.id.browser_open)
        button_map = findViewById(R.id.maps_open)
        button = findViewById(R.id.button)
        button_map.setOnClickListener(this)
        button_open_Browser.setOnClickListener(this)
        button.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.browser_open -> {
                Log.w("Tag", "Browser_Open")
                val intent = Intent(Intent.ACTION_VIEW,Uri.parse("http://developer.android.com"))
                startActivity(intent)
            }
            R.id.button -> {
                Log.w("Tag", "Send_Text_Field")
                val intent = Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+textField_name.text.toString()))
                startActivity(intent)
            }
            R.id.maps_open -> {
                Log.w("Tag", "Map Open")
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:55.754283,37.62002"))
                startActivity(intent)
            }


        }
    }
}