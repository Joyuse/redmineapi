package com.example.preference

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var textView_get: TextView
    lateinit var text_field: EditText
    lateinit var button_setnd:Button
    lateinit var button_get: Button
    lateinit var sharedPreferences: SharedPreferences
    lateinit var string_key_pref: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        string_key_pref = "Key"
        setContentView(R.layout.activity_main)
        text_field = findViewById(R.id.edit_text)
        textView_get = findViewById(R.id.insert_text)
        button_get = findViewById(R.id.get_Button)
        button_setnd = findViewById(R.id.send_button)
        button_setnd.setOnClickListener(this)
        button_get.setOnClickListener(this)
        get_text()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.get_Button -> {
                save_text()
            }
            R.id.send_button -> {
                get_text()
            }
        }
    }

    fun save_text() {
        sharedPreferences = getPreferences(MODE_PRIVATE)
        val editor  = sharedPreferences.edit()
        editor.putString(string_key_pref,text_field.text.toString()).apply()
        super.onDestroy()
    }

    fun get_text() {
        sharedPreferences = getPreferences(MODE_PRIVATE)
        val text_for_texView = sharedPreferences.getString(string_key_pref," ")
        textView_get.text = text_for_texView
    }
}