package com.example.a06

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text


class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var button: Button
    lateinit var buttonHard: Button
    lateinit var vextView: TextView
    lateinit var labelText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vextView = findViewById(R.id.wrapText)
        labelText = findViewById(R.id.lable_text)
        vextView.append("Some Text Append")
        button = findViewById(R.id.buttonId)
        button.setOnClickListener(this)
        buttonHard = findViewById(R.id.button_hard)
        buttonHard.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.buttonId -> {
                Toast.makeText(this,"Some String",Toast.LENGTH_SHORT).show()
                funButton(vextView)
            }
            R.id.button_hard -> {
                labelText.text = vextView.text
            }
        }
    }


    fun funButton (textView: TextView) {
        textView.text = "Some Text From Fun"
    }

}