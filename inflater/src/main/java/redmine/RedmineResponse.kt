package redmine

import com.google.gson.annotations.SerializedName

data class RedmineResponse(

	@field:SerializedName("projects")
	val projects: List<ProjectsItem?>? = null,

	@field:SerializedName("offset")
	val offset: Int? = null,

	@field:SerializedName("total_count")
	val totalCount: Int? = null,

	@field:SerializedName("limit")
	val limit: Int? = null
)

data class ProjectsItem(

	@field:SerializedName("updated_on")
	val updatedOn: String? = null,

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("created_on")
	val createdOn: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("is_public")
	val isPublic: Boolean? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
