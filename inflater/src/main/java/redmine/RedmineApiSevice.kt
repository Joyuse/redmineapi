package redmine

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RedmineApiSevice {
    fun getRepos(callback: Callback<RedmineResponse>){
        val retrofit = RedmineBuilder.buildService(RedmineService::class.java)
        retrofit?.getRedmineRepos()?.enqueue(
            callback
        )
    }
}