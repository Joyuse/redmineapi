package redmine

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path

interface RedmineService {
    @Headers("X-Redmine-API-Key:47165d54e839f3e39aec7ba5dbd6db8d8c844019")
    @GET("projects.json")
    fun getRedmineRepos(): Call<RedmineResponse>
}