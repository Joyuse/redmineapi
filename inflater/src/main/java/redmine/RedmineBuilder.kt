package redmine

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RedmineBuilder {
     val client = OkHttpClient.Builder().build()

     val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://rm.stagingmonster.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun buildService(service: Class<RedmineService>): RedmineService? {
        return retrofit.create(service)
    }
}