package com.example.inflater

import adapter.CustomAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import redmine.RedmineApiSevice as RedmineApiSevice
import redmine.RedmineResponse as RedmineResponse

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        funValues()
        setContentView(R.layout.activity_main)
    }

    private fun setUpAdapter(dataForAdapter: RedmineResponse?) {
        val customAdapter = CustomAdapter()
        val recycleView: RecyclerView = findViewById(R.id.recycleView)
        customAdapter.submitList(dataForAdapter?.projects)
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.adapter = customAdapter
        customAdapter.notifyDataSetChanged()
        Log.w("TAG","End setUpAdapter")
    }

    private fun funValues() {
        val callback = object : Callback<RedmineResponse> {
            override fun onFailure(call: Call<RedmineResponse>, t: Throwable) {
                Log.w("TAG", "HerTebe а не данные в этом ULTARA GYM 3000 CUMSHOTS ON YOUR FACE")
            }
            override fun onResponse(
                call: Call<RedmineResponse>,
                response: Response<RedmineResponse>
            ) {
                val value = response.body()
                setUpAdapter(value)
                Log.e("TAG", "" + value?.projects?.get(0)?.id)
            }
        }
        RedmineApiSevice().getRepos(callback)
    }
}