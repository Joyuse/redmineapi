package adapter

import androidx.recyclerview.widget.DiffUtil
import redmine.ProjectsItem
import redmine.RedmineResponse

class MyDiffUtils : DiffUtil.ItemCallback<ProjectsItem>() {
    override fun areItemsTheSame(oldItem: ProjectsItem, newItem: ProjectsItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ProjectsItem, newItem: ProjectsItem): Boolean {
        return oldItem == newItem
    }
}