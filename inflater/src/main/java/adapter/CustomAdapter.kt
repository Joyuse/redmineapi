package adapter

import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.inflater.R
import intarface.MyInterface
import redmine.ProjectsItem
import redmine.RedmineResponse


class CustomAdapter() : ListAdapter<ProjectsItem, CustomAdapter.ViewHolder>(MyDiffUtils()) {

    private var itemClickListener: MyInterface? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(p0.context).inflate(R.layout.projects_layout,p0,false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindValues(getItem(p1))
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
         var idId: TextView? = null
         var idIdentifier: TextView? = null
         var idDescription: TextView? = null
         var idStatus: TextView? = null
         var idIsPublic: TextView? = null
         var idCreated: TextView? = null
         var idLastUpdate: TextView? = null

        fun bindValues(value: ProjectsItem) {
            Log.w("TAG","Я РАБОТАЮ")
            idId = itemView.findViewById(R.id.id_count)
            idIdentifier = itemView.findViewById(R.id.id_identifier)
            idDescription = itemView.findViewById(R.id.id_description)
            idStatus = itemView.findViewById(R.id.id_status)
            idIsPublic = itemView.findViewById(R.id.id_public)
            idCreated = itemView.findViewById(R.id.id_created)
            idLastUpdate = itemView.findViewById(R.id.id_updated)

            idId?.text = value.id.toString()
            idIdentifier?.text = value.identifier.toString()
            idDescription?.text = value.description
            idStatus?.text = value.status.toString()
            idIsPublic?.text = value.status.toString()
            idCreated?.text = value.status.toString()
            idLastUpdate?.text = value.updatedOn.toString()

            itemView.setOnClickListener{
                itemClickListener?.onClickItem(value)
                Log.w("TAG", "value = ${value.name}")
            }
        }
    }

}