package com.example.a09

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var scaleButton: Button
    lateinit var fade_in: Button
    lateinit var fade_out: Button
    lateinit var rotate: Button
    lateinit var slide_down: Button
    lateinit var slide_up: Button
    lateinit var zoom_in:Button
    lateinit var zoom_out:Button
    lateinit var textView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.textView)
        scaleButton = findViewById(R.id.scaleButton)
        fade_in = findViewById(R.id.fade_in)
        fade_out = findViewById(R.id.fade_out)
        rotate = findViewById(R.id.rotate)
        slide_down = findViewById(R.id.slide_down)
        slide_up = findViewById(R.id.slide_up)
        zoom_in = findViewById(R.id.zoom_in)
        zoom_out = findViewById(R.id.zoom_out)
        scaleButton.setOnClickListener(this)
        fade_in.setOnClickListener(this)
        fade_out.setOnClickListener(this)
        rotate.setOnClickListener(this)
        slide_down.setOnClickListener(this)
        slide_up.setOnClickListener(this)
        zoom_in.setOnClickListener(this)
        zoom_out.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.scaleButton -> {

            }
            R.id.fade_in -> {
                textView.visibility = View.VISIBLE
                val animation = AnimationUtils.loadAnimation(this,R.anim.fade_in)
                textView.startAnimation(animation)
                Log.w("tag","msg")
            }
            R.id.fade_out -> {
                val animation = AnimationUtils.loadAnimation(this,R.anim.fade_out)
                textView.startAnimation(animation)
                Handler().postDelayed({
                    textView.visibility = View.GONE
                }, 1000)
                Log.w("tag","msg")
            }
            R.id.rotate -> {
                val animtion = AnimationUtils.loadAnimation(this, R.anim.rotate)
                Log.w("tag","msg")
            }
            R.id.slide_down -> {
                val animation = AnimationUtils.loadAnimation(this,R.anim.slide_down)
                textView.startAnimation(animation)
                Log.w("tag","msg")
            }
            R.id.slide_up -> {
                val animation = AnimationUtils.loadAnimation(this,R.anim.slide_up)
                textView.startAnimation(animation)
                Log.w("tag","msg")
            }
            R.id.zoom_in -> {
                val animation = AnimationUtils.loadAnimation(this,R.anim.zoom_in)
                textView.startAnimation(animation)
                Log.w("tag","msg")
            }
            R.id.zoom_out -> {
                val animation = AnimationUtils.loadAnimation(this,R.anim.zoom_out)
                textView.startAnimation(animation)
                Log.w("tag","msg")
            }
        }
    }
}